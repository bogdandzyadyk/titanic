import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    df['Title'] = df['Name'].str.extract(r' ([A-Za-z]+)\.')

    titles_to_consider = ["Mr.", "Mrs.", "Miss."]

    median_values = {}

    missing_values_count = {}

    for title in titles_to_consider:
        title_data = df[df['Title'] == title]

        median_age = title_data['Age'].median()

        missing_age_count = title_data['Age'].isnull().sum()

        median_values[title] = median_age
        missing_values_count[title] = missing_age_count

    median_values = {title: round(median) for title, median in median_values.items()}

    result = [(title, missing_values_count[title], median_values[title]) for title in titles_to_consider]

    return result
